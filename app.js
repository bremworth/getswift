/**
 * Written by Alexander Witting as a demo for GetSwift
 * http://www.getswift.co/
 * 4-8-17
 * alexwitting@gmail.com
 */


var drone_test = [{"droneId":19784,"location":{"latitude":-37.772199076297184,"longitude":144.85719322491778},"packages":[{"destination":{"latitude":-37.77982516021524,"longitude":144.8574276441187},"packageId":548,"deadline":1501813051}]},{"droneId":79397,"location":{"latitude":-37.76722512662835,"longitude":144.86271093618828},"packages":[{"destination":{"latitude":-37.77581073489103,"longitude":144.85825608699525},"packageId":1618,"deadline":1501814917}]},{"droneId":182457,"location":{"latitude":-37.77128782279899,"longitude":144.86323641668062},"packages":[{"destination":{"latitude":-37.76736643235777,"longitude":144.8488242629372},"packageId":3162,"deadline":1501814152}]},{"droneId":258156,"location":{"latitude":-37.77470147008008,"longitude":144.86089285459806},"packages":[{"destination":{"latitude":-37.772511500601176,"longitude":144.86236207348122},"packageId":5902,"deadline":1501814843}]},{"droneId":262239,"location":{"latitude":-37.76808243998624,"longitude":144.8541102692367},"packages":[{"destination":{"latitude":-37.78306600891704,"longitude":144.8513311212069},"packageId":6287,"deadline":1501813753}]},{"droneId":287274,"location":{"latitude":-37.780689181940545,"longitude":144.8601696246652},"packages":[{"destination":{"latitude":-37.78235563781404,"longitude":144.86259197620595},"packageId":6757,"deadline":1501815081}]},{"droneId":342761,"location":{"latitude":-37.766452782235746,"longitude":144.86307693091516},"packages":[{"destination":{"latitude":-37.769676463114884,"longitude":144.8561842898882},"packageId":6812,"deadline":1501814882}]},{"droneId":356244,"location":{"latitude":-37.77648086771614,"longitude":144.85908701400396},"packages":[{"destination":{"latitude":-37.78379618588465,"longitude":144.84896759843863},"packageId":7338,"deadline":1501815194}]},{"droneId":423514,"location":{"latitude":-37.771661165462135,"longitude":144.85258956374267},"packages":[{"destination":{"latitude":-37.77591519679109,"longitude":144.84729455617182},"packageId":7407,"deadline":1501815728}]},{"droneId":469394,"location":{"latitude":-37.76654530092324,"longitude":144.85805025148522},"packages":[{"destination":{"latitude":-37.768912601257696,"longitude":144.84834127952237},"packageId":7961,"deadline":1501813404}]},{"droneId":491759,"location":{"latitude":-37.778219587365555,"longitude":144.84815366302016},"packages":[]},{"droneId":512931,"location":{"latitude":-37.771059334389626,"longitude":144.85690859691272},"packages":[]},{"droneId":513438,"location":{"latitude":-37.7704159338522,"longitude":144.85723710098418},"packages":[]},{"droneId":598740,"location":{"latitude":-37.78459083347878,"longitude":144.86069614523586},"packages":[]},{"droneId":614649,"location":{"latitude":-37.77449256842038,"longitude":144.8633216148547},"packages":[]},{"droneId":631159,"location":{"latitude":-37.7794599089083,"longitude":144.86018654131473},"packages":[]},{"droneId":838236,"location":{"latitude":-37.78268073068871,"longitude":144.85199575784506},"packages":[]},{"droneId":862630,"location":{"latitude":-37.78142454340572,"longitude":144.84894381166464},"packages":[]},{"droneId":911494,"location":{"latitude":-37.78340097236636,"longitude":144.85462876090804},"packages":[]},{"droneId":913584,"location":{"latitude":-37.76722535837211,"longitude":144.86016839243268},"packages":[]}]
var package_test = [{"destination":{"latitude":-37.78382603956365,"longitude":144.8564097410011},"packageId":269,"deadline":1501819596},{"destination":{"latitude":-37.76578281820686,"longitude":144.85992145822502},"packageId":312,"deadline":1501819501},{"destination":{"latitude":-37.7716375896314,"longitude":144.85844880830535},"packageId":313,"deadline":1501820739},{"destination":{"latitude":-37.772832948942,"longitude":144.85999690685986},"packageId":756,"deadline":1501819849},{"destination":{"latitude":-37.7723849674141,"longitude":144.847352845346},"packageId":924,"deadline":1501820363},{"destination":{"latitude":-37.77902606178489,"longitude":144.85304940539226},"packageId":1003,"deadline":1501819459},{"destination":{"latitude":-37.77305688224738,"longitude":144.85245080199326},"packageId":1196,"deadline":1501819801},{"destination":{"latitude":-37.77970592776277,"longitude":144.85351308228292},"packageId":1559,"deadline":1501818322},{"destination":{"latitude":-37.78147041557861,"longitude":144.8559157985464},"packageId":1624,"deadline":1501820070},{"destination":{"latitude":-37.78137222734275,"longitude":144.85267987601813},"packageId":2175,"deadline":1501818976},{"destination":{"latitude":-37.7736668661234,"longitude":144.86465332505705},"packageId":2281,"deadline":1501820055},{"destination":{"latitude":-37.77468522644131,"longitude":144.86284639441197},"packageId":2404,"deadline":1501818705},{"destination":{"latitude":-37.78376064565509,"longitude":144.84802538319124},"packageId":2510,"deadline":1501819505},{"destination":{"latitude":-37.777519816801984,"longitude":144.85658892588154},"packageId":3081,"deadline":1501820469},{"destination":{"latitude":-37.76666164687933,"longitude":144.852176790954},"packageId":3171,"deadline":1501820705},{"destination":{"latitude":-37.7811762526544,"longitude":144.85659221799665},"packageId":3190,"deadline":1501820645},{"destination":{"latitude":-37.769945974268595,"longitude":144.8610005032951},"packageId":3480,"deadline":1501819209},{"destination":{"latitude":-37.78429723341958,"longitude":144.85231031549338},"packageId":3637,"deadline":1501819345},{"destination":{"latitude":-37.783588501789346,"longitude":144.84960927552973},"packageId":3723,"deadline":1501819952},{"destination":{"latitude":-37.783036356965766,"longitude":144.86052593955773},"packageId":4137,"deadline":1501818194},{"destination":{"latitude":-37.77148017247225,"longitude":144.86353805033306},"packageId":4341,"deadline":1501820523},{"destination":{"latitude":-37.78123591468644,"longitude":144.86025109498217},"packageId":4432,"deadline":1501820221},{"destination":{"latitude":-37.76856873543581,"longitude":144.85295167912346},"packageId":5169,"deadline":1501820413},{"destination":{"latitude":-37.768447397909185,"longitude":144.85078171707698},"packageId":5306,"deadline":1501818210},{"destination":{"latitude":-37.77695661263235,"longitude":144.85665303336083},"packageId":5523,"deadline":1501819651},{"destination":{"latitude":-37.7664145256472,"longitude":144.84877568053176},"packageId":6973,"deadline":1501820252},{"destination":{"latitude":-37.777984238058046,"longitude":144.85467819491782},"packageId":7015,"deadline":1501820225},{"destination":{"latitude":-37.765881281599555,"longitude":144.85339400540394},"packageId":7126,"deadline":1501818221},{"destination":{"latitude":-37.77624605656253,"longitude":144.86308808522492},"packageId":7228,"deadline":1501820871},{"destination":{"latitude":-37.769061858659214,"longitude":144.86102314287476},"packageId":7374,"deadline":1501818006},{"destination":{"latitude":-37.76595245622192,"longitude":144.85177288620017},"packageId":7560,"deadline":1501818679},{"destination":{"latitude":-37.77545776847812,"longitude":144.85627618703543},"packageId":7568,"deadline":1501820008},{"destination":{"latitude":-37.76879570654571,"longitude":144.85274743277387},"packageId":7767,"deadline":1501820914},{"destination":{"latitude":-37.77015971922951,"longitude":144.85427665133759},"packageId":7917,"deadline":1501819227},{"destination":{"latitude":-37.77562234164538,"longitude":144.85673508300437},"packageId":8260,"deadline":1501818168},{"destination":{"latitude":-37.773396823488355,"longitude":144.85233388447847},"packageId":8571,"deadline":1501818611},{"destination":{"latitude":-37.772975915287454,"longitude":144.8633648649897},"packageId":8740,"deadline":1501820870},{"destination":{"latitude":-37.783851590180205,"longitude":144.85652162625144},"packageId":8855,"deadline":1501818647},{"destination":{"latitude":-37.76507453446835,"longitude":144.8539213463172},"packageId":8912,"deadline":1501818854},{"destination":{"latitude":-37.76772346813772,"longitude":144.85394252061354},"packageId":9004,"deadline":1501817943},{"destination":{"latitude":-37.76894574074117,"longitude":144.84953005812628},"packageId":9335,"deadline":1501819063},{"destination":{"latitude":-37.77816125115614,"longitude":144.8635225443717},"packageId":9696,"deadline":1501820441},{"destination":{"latitude":-37.78252927683506,"longitude":144.8482184142299},"packageId":9712,"deadline":1501819837}]

/** Require */
var restify = require('restify');
var TimSort = require('timsort');
var async = require('async');


/** Globals */
var depot_location = {'latitude' : -37.816653, 'longitude' : 144.963848 }; //'303 Collins Street, Melbourne, Vic 3000'; // ??
var default_weight = 1;
var default_speed = 50; //50km

/** APP */

//Restify REST API service for building the API calls

var client = restify.createJsonClient({
    url: 'https://codetest.kube.getswift.co',
    version: '*'
});

/** REST Client */

//GET drones
function getDrones(callback) {
    var requestURL = '/drones'
    
    //Make the API call to the server for the list of available packages
    client.get(requestURL, function (err, req, res, obj) {
        if(err) {
            console.log('Could not make the API call for packages');
            return callback(err);
        }
        //sort by distance
        TimSort.sort(obj, sortDrones)
        return callback(obj);
    });
}

//TimSort implemtation with deep comparisons 
function sortDrones(droneA, droneB) {
    //Package weight, speed, range,
    droneA['speed'] = default_speed;
    droneB['speed'] = default_speed;
    
    if(droneA.distance == null)
        getDroneDistance(droneA)
    if(droneB.distance == null)
        getDroneDistance(droneB)
             
    //The distance of the drone is also sorted by the speed of the drone. 
    return (droneA['distance']/droneA.speed*1000) < (droneB['distance']/droneB.speed*1000) ? -1 : 1;
    
}

//GET packages
function getPackages(callback) {
    var requestURL = '/packages'
    
    //Make the API call to the server for the list of available packages
    client.get(requestURL, function (err, req, res, obj) {
        if(err) {
            console.log('Could not make the API call for packages');
            return callback(err);
        }
        
        //Sort by distance
        TimSort.sort(obj, sortPackages)
        
        return callback(obj);
    });
}

//TimSort implemtation with deep comparisons 
function sortPackages(packageA, packageB) {
    //Defaults
    packageA['weight'] = default_weight;
    packageB['weight'] = default_weight;
    
    packageA['distance'] = calcDist(packageA.destination, depot_location);
    packageB['distance'] = calcDist(packageB.destination, depot_location);
    
    
    //Multiplied by the weight modifier. 
    return ((packageA['distance'] * packageA.weight) < (packageB['distance']*packageB.weight)) ? 1 : -1;
   
}
/** Dispatcher */

//There may be more packages then drones or vice versa
function dispatcher(drones, packages) {
    
    var debug = false;

    //Testing, dummy data 
    if(debug) {
        TimSort.sort(drone_test, sortDrones);
        TimSort.sort(package_test, sortPackages);
        var drones = drone_test;
        var packages = package_test;
        console.log(drones)
        console.log(packages)
    } else {
        async.parallel([    
            function(callback) {
                getDrones(function(results) {
                    callback(null, results);
                });
            },
            function(callback) {
                getPackages(function(results) {
                    callback(null,results);
                });
            }
       ],
        function(err, results) {
            assignment(results[0], results[1])
        })
    }

    
}


function assignment(drones, packages) {
    
    
    var assigned = []
    var unassigned = []
    var d = 0;
    var currentTime = Date.now() 
    
    //Go through the packages, we are only interested in the packages we have available
    for(var p in packages) {
        
        if(d < drones.length) {
            //Time left to deliver before the deadline 
            var time_to_deliver = packages[p].deadline*1000 - currentTime;
            var delivery_time =  ((drones[d].distance / drones[d].speed * 1000))
            
            if(time_to_deliver < delivery_time ) {
                //The current package delivery deadline cannot be met by any drone 
                unassigned.push(packages[p].packageId)
                continue
            }
            //Create a job assignment from the drone and package
            var job = {droneId : drones[d].droneId, packageId : packages[p].packageId}
            //Add to the job queue
            assigned.push(job)
            //Next drone
            d++;
        } else {
            //Out of drones, push the rest of the packageId's to the unassigned
            unassigned.push(packages[p].packageId)
        }
    }
    //Build the final dispatch assignment for output, either back to the API or in this case to stdout
    var dispatch = {assignments: assigned, unassignedPackageIds: unassigned}
    //Handle output
    console.log(dispatch)
} 




/** Util */

//If a drone is already on a job calculate it's distance and time till completion so it can be added to the 
//available drones queue
function getDroneDistance(drone) { 
    if(drone['packages'].length > 0) {
        var distance_to_package = calcDist(drone['packages'][0].destination, drone.location);
        var package_from_depot = calcDist(drone['packages'][0].destination, depot_location);
        drone['distance'] = distance_to_package + package_from_depot;
    } else {
        drone['distance'] = calcDist(drone.location, depot_location);
    }      
}


function toRadians(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
}
//Calculate Distance between two lat long points using the Haversine formula to extract distance in metres
//https://en.wikipedia.org/wiki/Haversine_formula
function calcDist(locA, locB) {
 
    if(locA['latitude'] == locB['latitude'] && locA['longitude'] == locB['longitude']) //Drone is at depot?
        return 0;
    
    var R = 6371e3; //metres
    var lat1 = toRadians(locA['latitude']);
    var lat2 = toRadians(locB['latitude']);
    var delta1 = toRadians(lat2 - lat1);
    var delta2 = toRadians(locB['longitude'] - locA['longitude']);
    
    var a = Math.sin(delta1 / 2) * Math.sin( delta1 / 2) + 
            Math.cos(lat1) * Math.cos(lat2) *
            Math.sin(delta2 / 2) * Math.sin(delta2 / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    
    var d = R * c;
    return d; //metres
}


dispatcher();



